// SPDX-License-Identifier: GPL-3.0

pragma solidity  > 0.6.0 < 0.9.0 ;


contract SimpleStorage{
    
    // inizializziamo a zero la variabile
    //altro commento
    uint256 favoriteNumvber;
    /*
    bool favoriteBool = true;
    string favoriteString = "String";
    int256 favoriteInt = -5;
    address favorteAddress = xkjkdsljflksdjfksjdfklsjdf;
    bytes32 favorteBytes = "cat";
    */

    struct People{
        uint256 favoriteNumvber;
        string name;
    }

    People[] public people;
    mapping(string => uint256) public nameToFavoriteNumber;

    function addPerson(string memory _name, uint256 _favoriteNumber) public{
        //Metodo per inserire un oggetto con i riferimenti ai parametri stile Python
        //people.push(People({favoriteNumvber: _favoriteNumber, name: _name}));
        //Metodo per inserire un oggetto rispettando il posizionamento della struttura
        //stile Java code
        people.push(People( _favoriteNumber, _name));
        nameToFavoriteNumber[_name] = _favoriteNumber;
    } 

    function store(uint256 _favoriteNumber) public {
        favoriteNumvber = _favoriteNumber;
    }

    function retrieve() public view returns(uint256){
        return favoriteNumvber;

    }


}